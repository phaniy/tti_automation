package com.knacksystems.driverfactory;

import java.io.FileNotFoundException;

import com.knacksystems.utilities.TestcasesValidation;

import junit.framework.Assert;

public class BuildStatus 
{
 public void finalBuild() throws FileNotFoundException, Throwable
 {
	 int tc_cnt=0, act_cnt=0;
	 TestcasesValidation tc=new TestcasesValidation();
	 for(int i=1;i<=tc.rowCount("MasterTestCases");i++)
	 {
		 String validation=tc.getData("MasterTestCases", i, 3);
		 System.out.println("Testcase result: "+validation);
		 System.out.println("Row Number: "+i);
		 if(validation.equalsIgnoreCase("Pass"))
		 {
			 tc_cnt++;
			 act_cnt++;
		 }
		 else if(validation.equalsIgnoreCase("Fail"))
		 {
			 tc_cnt++;
		 }
		 else
		 {
			 System.out.println("Not Executed");
		 }
	 }
	 Assert.assertEquals(tc_cnt, act_cnt);
 }
}